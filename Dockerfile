FROM openjdk:8
MAINTAINER foching
ADD target/demo.war  demo.war
EXPOSE 8085  
ENTRYPOINT ["java","-jar","demo.war"]