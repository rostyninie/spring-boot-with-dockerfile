package com.exemple.demo.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "t_user")
public class User extends AbstractIdentifiantIntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2558135662488257271L;

	@Column(name ="user_name")
	private String userName;
	
	private String password;
	private String email;
	
	@ManyToMany(fetch = FetchType.EAGER, targetEntity = Role.class, cascade =  CascadeType.ALL)
	@JoinTable(name="t_user_role", joinColumns = @JoinColumn(name="user_id", foreignKey = @ForeignKey(name ="fk_user_role")),
	inverseJoinColumns =  @JoinColumn(name="role_id", foreignKey = @ForeignKey(name ="fk_role_user")))
	private Set<Role> roles;
	
	@ManyToOne(fetch =  FetchType.EAGER, targetEntity = Groupe.class)
	@JoinColumn(name="groupe_id", foreignKey = @ForeignKey(name = "fk_user_groupe"))
	private Groupe groupe;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}
	
	
	
	

}
