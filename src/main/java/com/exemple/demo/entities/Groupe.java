package com.exemple.demo.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="t_groupe")
public class Groupe extends AbstractIdentifiantIntity  {

	private String name;
	
	@OneToMany(fetch = FetchType.EAGER, targetEntity = User.class, cascade= CascadeType.ALL, mappedBy="groupe")
	private Set<User> users;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	
}
