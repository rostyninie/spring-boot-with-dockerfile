package com.exemple.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.exemple.demo.entities.AbstractIdentifiantIntity;

public interface DemoRepositry<T extends AbstractIdentifiantIntity> extends CrudRepository<T, Long> {
	
	//public T findOne(Long id);
	
	public T save(T object);
	
	public Iterable<T> findAll();
	
	public void delete(T object);
	
	public void deleteAll();

}
