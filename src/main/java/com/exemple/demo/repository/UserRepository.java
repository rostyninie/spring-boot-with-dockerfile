package com.exemple.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import com.exemple.demo.entities.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
