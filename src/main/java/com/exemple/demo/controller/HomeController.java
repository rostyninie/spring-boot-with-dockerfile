package com.exemple.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exemple.demo.entities.User;
import com.exemple.demo.repository.UserRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("hello/")
public class HomeController {
	Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	UserRepository userRepository;
	
    @ApiOperation(value = "To save the Order")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Order is successfully created"),
            @ApiResponse(code = 404, message = "Invalid URL"),
            @ApiResponse(code = 403, message = "Forbidden")
    })
	@GetMapping("/")
	public List<User> getAll() {
		
		
		return (ArrayList<User>)userRepository.findAll();
	}
	
	@PostMapping
	public User saveUser(@RequestBody User user) {
		logger.info("Je sauvegarde user");
		return userRepository.save(user);
		
	}

}
