package com.exemple.demo;



import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.exemple.demo.entities.User;
import com.exemple.demo.repository.UserRepository;
@ExtendWith(SpringExtension.class)
@SpringBootTest
class DemoApplicationTests {

	@Autowired
	UserRepository userRepository;
	
	private User user;
	
	@BeforeEach
	public void initUse() {
		user =new User();
		user.setUserName("Foching");
		user.setPassword("Rost");
		user.setEmail("tat");
	}
	
	@Test
	public void saveUser() {
		user =userRepository.save(user);
		assertNotNull(Long.valueOf(user.getId()));
	}

}
